<%

'/**
' * 7384公交查询系统 v5.0(LastEdit:2016-08-02) 
' * ------busdanV5.0:city.asp----------------------------------------------------------------------
' * 在线演示 【http://dan.96448.cn】 (<---等待你的访问哦)。
' * 版权所有 【www.12391.net】 (<---等待你的访问哦)。
' * 承接各种查询系统定做：简单，通用，易用。
' * $Author: yujianyue <admin@ewuyi.net> $
' * $LastEdit: 2016-08-02 $
' * ======busdanV5.0:city.asp======================================================================
' * 本系统改进者先后免费公布了以下五大系列超过12款代码

' * 大家都可以百度下得到下载地址(部分未收录)：

' * asp+txt（手机版/电脑版 ：2款） 

' * 详情百度：asp+txt 通用查询系统 手机版/电脑版 / asp+txt 员工工资查询系统
' * asp+excel(xls) （手机版/电脑版 ：2款）: 

' * 详情百度：asp+excel多用途查询系统 手机版/电脑版 / asp+excel图书在线检索系统 手机版/电脑版
' * php+txt (Linux/windows 手机版/电脑版 ：4款) 

' * 详情百度：php+txt多用途查询系统 手机版/电脑版 / php+txt 成绩查询系统通用版 手机版/电脑版
' * php+csv (windows：1款 后续还有) 

' * 详情百度: php+csv(Excel)通用成绩查询系统
' * php+excel(xls) (Linux/windows 手机版/电脑版：4款) 

' * 详情百度：php+excel通用成绩查询系统 手机版/电脑版 / php+excel多用途查询系统 / php+excel通用课表查询系统
'*/

'======================================
'--------------------------------------
'(C)2011-2099 Ewuyi.Net
'HomePage:http://www.7384.org/
'HttpSite:http://buy.7384.org/
'HttpSite:http://buy.Ewuyi.Net/
'MyEmail1:Admin@Ewuyi.Net
'MyEmail2:Admin@Ewuyi.Net
'--------------------------------------
'======================================
if len(db)<3 then
response.write "<fieldset class='lblock'><legend>错误信息</legend>非法访问。</fieldset>"
response.End()
end if
%><!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=city%>公交网,<%=city%>公交查询,<%=city%>公交线路查询,<%=city%>公交车换乘查询</title><meta name="author" content="yujianyue,admin@ewuyi.net">
<meta property="qc:admins" content="a50a939f94e264910aa6ab1df82eaf2b" />
<meta name="copyright" content="www.96448.cn">
<meta name="baidu-site-verification" content="30277a10971afc89edf3437df005bf10" />
<meta http-equiv="Content-Language" content="zh-CN" />
<meta http-equiv="x-ua-compatible" content="ie=7" />
<meta name="Keywords" content="<%=city%>公交,<%=city%>公交查询,<%=city%>公交线路查询" />
<meta name="Description" content="<%=city%>公交网,<%=city%>公交查询,<%=city%>公交线路查询,<%=city%>公交车查询服务." />
<%call header("H")%>
<div class="main">
 <div class="mainLeft">
<div class="ltitle">
<a href="<%=sitedir%>?_l1.html" class="fr"><%=city%>所有线路</a>
<a href="<%=sitedir%>?_CC.html" class="fr"><%=city%>所有站点</a>
<a href="http://7384.org/">返回全国公交网</a></div>
 <div class="llist">
 <%=tishi(""&city&"线路查询记录",listhis("xian",""),"zhan")%>
 <%=tishi(""&city&"站点查询记录",listhis("zhan",""),"zhan")%>
 <%=tishi(""&city&"换乘方案查询记录",listhis("huan",""),"zhan")%>
<fieldset class='lblock zhan'>
 <legend>广告：特别说明</legend>
 <%call ad72817()%>
</fieldset>
</div></div>
 <div class="mainRight"> 
 <div class="sideNav sideNavd"><span class="spanL">广告也精彩哦</span></div>
 <div class="sideList"><%call ad72816()%></div></div>
</div><div class="clear"></div></div>
<%call footer()%>

<!--a50a939f94e264910aa6ab1df82eaf2b-->
<!--
通用成绩查询系统解决方案(简单通用易用):

方案4:微信公众号N选1个查询条件直接查询工资、成绩、水电费等

自助开通试用:http://new.12391.net/ 
视频教程下载:http://pan.baidu.com/s/1ge6BPEr 
代码购买:https://item.taobao.com/item.htm?id=520496908275 
整体服务:https://item.taobao.com/item.htm?id=529624346797 
方案3(荐):微信公众号一对一绑定才可以查询工资、成绩、水电费等

自助开通试用:http://add.96cha.com/ 
代码购买:https://item.taobao.com/item.htm?id=44248394675 
整体服务:https://item.taobao.com/item.htm?id=528187132312 
方案2(荐):用户在线登录查询工资成绩水电费等，可自助修改密码

自助开通试用:http://add.dbcha.com/ 
视频教程下载:http://pan.baidu.com/s/1boANMwv 
代码购买:https://item.taobao.com/item.htm?id=43193387085 
整体服务:https://item.taobao.com/item.htm?id=528108807297 
方案1:直接通过设定的（1-3个）查询条件查询

免费即时开通:http://add.12391.net/ 
视频教程下载:http://pan.baidu.com/s/1eSoDn26 
代码购买:https://item.taobao.com/item.htm?id=528692002051 
整体服务:https://item.taobao.com/item.htm?id=520023732507 
代码版：不加密，无域名限制，无时间限制,一次付费一直可用(域名和网站空间费用另外自理)

整体服务：无需域名 无需空间 无需代码 无需技术人员 无需备案，即开即用 ,按时间付费
通用模糊检索系统解决方案(简单通用易用):

视频教程下载: http://pan.baidu.com/s/1jHOl9I2 或http://pan.baidu.com/s/1dEF8ppf

方案1:通用多选一模糊查询系统单输入框版

自助开通试用:http://add.xuelikai.com:1111/ 
视频教程下载:http://pan.baidu.com/s/1ge6BPEr (只参考第一步第二步)
代码购买:https://item.taobao.com/item.htm?id=520167788658 
方案2:通用多选一模糊查询系统下拉可选条件版

自助开通试用:http://add.xuelikai.com:2222/
视频教程下载:http://pan.baidu.com/s/1ge6BPEr (只参考第一步第二步)
代码购买:https://item.taobao.com/item.htm?id=520167788658 
方案3(荐):通用多选一模糊查询系统多输入框版

自助开通试用:http://add.xuelikai.com:3333/
视频教程下载:http://pan.baidu.com/s/1ge6BPEr (只参考第一步第二步)
代码购买:https://item.taobao.com/item.htm?id=520167788658 
-->
</body></html>

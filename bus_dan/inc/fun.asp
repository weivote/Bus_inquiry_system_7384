<%

'/**
' * 7384公交查询系统 v5.0(LastEdit:2016-08-02) 
' * ------busdanV5.0:fun.asp----------------------------------------------------------------------
' * 在线演示 【http://dan.96448.cn】 (<---等待你的访问哦)。
' * 版权所有 【www.12391.net】 (<---等待你的访问哦)。
' * 承接各种查询系统定做：简单，通用，易用。
' * $Author: yujianyue <admin@ewuyi.net> $
' * $LastEdit: 2016-08-02 $
' * ======busdanV5.0:fun.asp======================================================================
' * 本系统改进者先后免费公布了以下五大系列超过12款代码

' * 大家都可以百度下得到下载地址(部分未收录)：

' * asp+txt（手机版/电脑版 ：2款） 

' * 详情百度：asp+txt 通用查询系统 手机版/电脑版 / asp+txt 员工工资查询系统
' * asp+excel(xls) （手机版/电脑版 ：2款）: 

' * 详情百度：asp+excel多用途查询系统 手机版/电脑版 / asp+excel图书在线检索系统 手机版/电脑版
' * php+txt (Linux/windows 手机版/电脑版 ：4款) 

' * 详情百度：php+txt多用途查询系统 手机版/电脑版 / php+txt 成绩查询系统通用版 手机版/电脑版
' * php+csv (windows：1款 后续还有) 

' * 详情百度: php+csv(Excel)通用成绩查询系统
' * php+excel(xls) (Linux/windows 手机版/电脑版：4款) 

' * 详情百度：php+excel通用成绩查询系统 手机版/电脑版 / php+excel多用途查询系统 / php+excel通用课表查询系统
'*/

'======================================
'--------------------------------------
'(C)2011-2099 Ewuyi.Net
'HomePage:http://www.7384.org/
'HttpSite:http://buy.7384.org/
'HttpSite:http://buy.Ewuyi.Net/
'MyEmail1:Admin@Ewuyi.Net
'MyEmail2:Admin@Ewuyi.Net
'--------------------------------------
'======================================

response.Expires=-999
Session.Timeout=200
'判断数字 
Function AlertNum(CheckStr)
If Not IsNumeric(CheckStr) Or CheckStr="" Then
AlertNum="y"
End If
End Function

numstr="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz12345678"

Function chartoNum(numstr,char)
 '三位字母转数字函数,即26进制转10进制
 numstr_="@"&numstr
 lenstr =len(numstr)
 char1=len(split(numstr_,mid(char,1,1))(0))
 char2=len(split(numstr_,mid(char,2,1))(0))
 char3=len(split(numstr_,mid(char,3,1))(0))
 charNum=Cint(char1)*lenstr*lenstr+Cint(char2)*lenstr+Cint(char3)
 chartoNum=charNum-lenstr*lenstr-lenstr
End Function

Function Numtochar(numstr,num)
 num_5=len(numstr)*len(numstr)+num+len(numstr)
 num_1=num mod len(numstr)
 if num_1=0 then num_1=len(numstr)
 num_4=len(numstr)*len(numstr)
 num_3=num+num_4-1
 num_3=int(num_3/num_4)
 if num mod len(numstr)*len(numstr)=0 then num_2=len(numstr)
 if num_1=len(numstr) then
 num_2=num+num_4
 else
 num_2=num+num_4+len(numstr)
 end if
 num_2=int((num_2 mod num_4)/len(numstr))
 if num_2 mod len(numstr)=0 then num_2=len(numstr)
 Numchar=mid(numstr,Num_3,1)&""&mid(numstr,Num_2,1)&""&mid(numstr,Num_1,1)&""
 Numtochar=Numchar
End Function

Function Newstring(SourceCode,strng)
  Newstring=Instr(lcase(SourceCode),lcase(strng))
  if Newstring<=0 then Newstring=Len(SourceCode)
End Function

function getNum(str) 
 dim re 
 set re=new RegExp 
 re.pattern="\D" 
 re.global=true 
 getNum = re.replace(str, "") 
 end function

function tishi(title,texter,classs) 
classs=" "&classs&""
 tishi="<fieldset class='lblock"&classs&"'>"&vbcrlf
 tishi=tishi&"<legend>"&title&"</legend>"&vbcrlf
 tishi=tishi&""&texter&"</fieldset>"&vbcrlf
end function

'---------------------
'定义常量
'---------------------
set rs=server.createobject("adodb.recordset")
set rst=server.createobject("adodb.recordset")
set rs1=server.createobject("adodb.recordset")
'---------------------
'模糊猜词
'---------------------
function cf(str)
cf=""
if len(str)>2 then
m=len(str)-1
 For i = 1 to m
 If i = 1 Then 
cf = cf&"(z_char like '%"&c(mid(str,i,2))&"%' or z_char like '%"&del(c(mid(str,i,2)))&"%' or"
 ElseIf i = m Then 
cf = cf&" z_char like '%"&c(mid(str,i,2))&"%' or z_char like '%"&del(c(mid(str,i,2)))&"%')"
 Else 
cf = cf&" z_char like '%"&c(mid(str,i,2))&"%' or z_char like '%"&del(c(mid(str,i,2)))&"%' or"
 End If
 Next
else
cf=" z_char like '%"&c(str)&"%' or z_char like '%"&del(c(str))&"%'"
end if
end function

'---------------------
'字符转换函数
'---------------------
Function HTMLEncode(fString)
 If Not IsNull(fString) Then
fString = replace(fString, ">", "")
fString = replace(fString, "<", "")
fString = Replace(fString, CHR(32), "")
fString = Replace(fString, CHR(34), "")
fString = Replace(fString, CHR(39), "")
fString = Replace(fString, CHR(9), "")
fString = Replace(fString, CHR(13), "")
fString = Replace(fString, CHR(10) & CHR(10), "")
fString = Replace(fString, CHR(10), "")
HTMLEncode = fString
 End If
End Function

FrSql_In = "'|*|and |exec |or |insert |select |delete |update |count |master |truncate |declare |and	|exec	|insert	|select	|delete	|update	|count	|master	|truncate	|declare	|char(|mid(|chr(|and[|exec[|insert[|select[|delete[|update[|count[|master[|truncate[|declare[|set[|set |set	|where[|where |where	|xp_cmdshell|xp_cmdshell |xp_cmdshell	"
FrSql_Inf = split(FrSql_In,"|")
	'防止Get方法注入
If Request.QueryString<>"" Then
For FrSql_Xh=0 To Ubound(FrSql_Inf)
If Instr(LCase(Request.QueryString),FrSql_Inf(FrSql_Xh))<>0 Then
Response.Write "<Script Language=javascript>alert('请不要在参数中包含非法字符尝试注入');history.back(-1)</Script>"  
Response.end
End If
Next
End If
Sub ad72817()
%><script>playad("AD560tb");</script><%

'/**
' * 7384公交查询系统 v5.0(LastEdit:2016-08-02) 
' * ------busdanV5.0:fun.asp----------------------------------------------------------------------
' * 在线演示 【http://dan.96448.cn】 (<---等待你的访问哦)。
' * 版权所有 【www.12391.net】 (<---等待你的访问哦)。
' * 承接各种查询系统定做：简单，通用，易用。
' * $Author: yujianyue <admin@ewuyi.net> $
' * $LastEdit: 2016-08-02 $
' * ======busdanV5.0:fun.asp======================================================================
' * 本系统改进者先后免费公布了以下五大系列超过12款代码

' * 大家都可以百度下得到下载地址(部分未收录)：

' * asp+txt（手机版/电脑版 ：2款） 

' * 详情百度：asp+txt 通用查询系统 手机版/电脑版 / asp+txt 员工工资查询系统
' * asp+excel(xls) （手机版/电脑版 ：2款）: 

' * 详情百度：asp+excel多用途查询系统 手机版/电脑版 / asp+excel图书在线检索系统 手机版/电脑版
' * php+txt (Linux/windows 手机版/电脑版 ：4款) 

' * 详情百度：php+txt多用途查询系统 手机版/电脑版 / php+txt 成绩查询系统通用版 手机版/电脑版
' * php+csv (windows：1款 后续还有) 

' * 详情百度: php+csv(Excel)通用成绩查询系统
' * php+excel(xls) (Linux/windows 手机版/电脑版：4款) 

' * 详情百度：php+excel通用成绩查询系统 手机版/电脑版 / php+excel多用途查询系统 / php+excel通用课表查询系统
'*/

End Sub
Sub ad72816()
%><a href="http://add.12391.net/" target="_blank">
<img src="bus_dan/inc/ad250.jpg" alt="广告很精彩哦" border="0" align="absmiddle" width='100%' /></a>
<script>playad("AD250250");</script><%

'/**
' * 7384公交查询系统 v5.0(LastEdit:2016-08-02) 
' * ------busdanV5.0:fun.asp----------------------------------------------------------------------
' * 在线演示 【http://dan.96448.cn】 (<---等待你的访问哦)。
' * 版权所有 【www.12391.net】 (<---等待你的访问哦)。
' * 承接各种查询系统定做：简单，通用，易用。
' * $Author: yujianyue <admin@ewuyi.net> $
' * $LastEdit: 2016-08-02 $
' * ======busdanV5.0:fun.asp======================================================================
' * 本系统改进者先后免费公布了以下五大系列超过12款代码

' * 大家都可以百度下得到下载地址(部分未收录)：

' * asp+txt（手机版/电脑版 ：2款） 

' * 详情百度：asp+txt 通用查询系统 手机版/电脑版 / asp+txt 员工工资查询系统
' * asp+excel(xls) （手机版/电脑版 ：2款）: 

' * 详情百度：asp+excel多用途查询系统 手机版/电脑版 / asp+excel图书在线检索系统 手机版/电脑版
' * php+txt (Linux/windows 手机版/电脑版 ：4款) 

' * 详情百度：php+txt多用途查询系统 手机版/电脑版 / php+txt 成绩查询系统通用版 手机版/电脑版
' * php+csv (windows：1款 后续还有) 

' * 详情百度: php+csv(Excel)通用成绩查询系统
' * php+excel(xls) (Linux/windows 手机版/电脑版：4款) 

' * 详情百度：php+excel通用成绩查询系统 手机版/电脑版 / php+excel多用途查询系统 / php+excel通用课表查询系统
'*/

End Sub
Sub ad72815()
%><script>playad("AD72815");</script><%

'/**
' * 7384公交查询系统 v5.0(LastEdit:2016-08-02) 
' * ------busdanV5.0:fun.asp----------------------------------------------------------------------
' * 在线演示 【http://dan.96448.cn】 (<---等待你的访问哦)。
' * 版权所有 【www.12391.net】 (<---等待你的访问哦)。
' * 承接各种查询系统定做：简单，通用，易用。
' * $Author: yujianyue <admin@ewuyi.net> $
' * $LastEdit: 2016-08-02 $
' * ======busdanV5.0:fun.asp======================================================================
' * 本系统改进者先后免费公布了以下五大系列超过12款代码

' * 大家都可以百度下得到下载地址(部分未收录)：

' * asp+txt（手机版/电脑版 ：2款） 

' * 详情百度：asp+txt 通用查询系统 手机版/电脑版 / asp+txt 员工工资查询系统
' * asp+excel(xls) （手机版/电脑版 ：2款）: 

' * 详情百度：asp+excel多用途查询系统 手机版/电脑版 / asp+excel图书在线检索系统 手机版/电脑版
' * php+txt (Linux/windows 手机版/电脑版 ：4款) 

' * 详情百度：php+txt多用途查询系统 手机版/电脑版 / php+txt 成绩查询系统通用版 手机版/电脑版
' * php+csv (windows：1款 后续还有) 

' * 详情百度: php+csv(Excel)通用成绩查询系统
' * php+excel(xls) (Linux/windows 手机版/电脑版：4款) 

' * 详情百度：php+excel通用成绩查询系统 手机版/电脑版 / php+excel多用途查询系统 / php+excel通用课表查询系统
'*/

End Sub
Sub ad72890()
%>
<div class="adl"><script>playad("AD16090");</script></div>
<div class="adr"><script>playad("AD72890");</script></div>
<%

'/**
' * 7384公交查询系统 v5.0(LastEdit:2016-08-02) 
' * ------busdanV5.0:fun.asp----------------------------------------------------------------------
' * 在线演示 【http://dan.96448.cn】 (<---等待你的访问哦)。
' * 版权所有 【www.12391.net】 (<---等待你的访问哦)。
' * 承接各种查询系统定做：简单，通用，易用。
' * $Author: yujianyue <admin@ewuyi.net> $
' * $LastEdit: 2016-08-02 $
' * ======busdanV5.0:fun.asp======================================================================
' * 本系统改进者先后免费公布了以下五大系列超过12款代码

' * 大家都可以百度下得到下载地址(部分未收录)：

' * asp+txt（手机版/电脑版 ：2款） 

' * 详情百度：asp+txt 通用查询系统 手机版/电脑版 / asp+txt 员工工资查询系统
' * asp+excel(xls) （手机版/电脑版 ：2款）: 

' * 详情百度：asp+excel多用途查询系统 手机版/电脑版 / asp+excel图书在线检索系统 手机版/电脑版
' * php+txt (Linux/windows 手机版/电脑版 ：4款) 

' * 详情百度：php+txt多用途查询系统 手机版/电脑版 / php+txt 成绩查询系统通用版 手机版/电脑版
' * php+csv (windows：1款 后续还有) 

' * 详情百度: php+csv(Excel)通用成绩查询系统
' * php+excel(xls) (Linux/windows 手机版/电脑版：4款) 

' * 详情百度：php+excel通用成绩查询系统 手机版/电脑版 / php+excel多用途查询系统 / php+excel通用课表查询系统
'*/

End Sub
Sub header(tps)
%><meta http-equiv="Content-Type" content="text/html; charset=GB2312">
<link href="<%=sitedir%>bus_dan/css/main.css" type="text/css" rel="stylesheet">
<script language="javascript" src="<%=sitedir%>bus_dan/css/main.js" type="text/javascript"></script>
<script language="javascript" src="<%=sitedir%>bus_dan/css/ajax.js" type="text/javascript"></script>
<script language="javascript" src="<%=sitedir%>bus_dan/css/suggest.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">var Suggest = new Suggest();</script>
<script language="javascript" type="text/javascript">var sitedir = "<%=sitedir%>";</script>
</head>
<body>
<div class="head"><span style="float:left;">
<a href="http://7384.org/?_S.html" style="color:red;">全国公交网</a></span>
<a href="#" onclick="javascript:window.print()">打印该页</a>
<a href="javascript:void(0)" onclick="copyLink()">推荐好友</a>
<a href="javascript:void(0)" onclick="myhomepage()" name="homepage">设为首页</a>
<a href="javascript:void(0)" onclick="addfavorite()">加入收藏</a></div>
<div class="logoWarp">
<div class="logo">
<a href="<%=sitedir%>" title="<%=city%>公交网" style="font-size:<%=66-len(city)*5%>px;"><%=city%>公交网</a></div>
<div class="topGuild">
<script language="javascript" src="<%=sitedir%>bus_dan/css/top<%=tps%>.js" type="text/javascript"></script>
</div></div><div class="ad"><%call ad72890()%></div><div class="clear"></div><%

'/**
' * 7384公交查询系统 v5.0(LastEdit:2016-08-02) 
' * ------busdanV5.0:fun.asp----------------------------------------------------------------------
' * 在线演示 【http://dan.96448.cn】 (<---等待你的访问哦)。
' * 版权所有 【www.12391.net】 (<---等待你的访问哦)。
' * 承接各种查询系统定做：简单，通用，易用。
' * $Author: yujianyue <admin@ewuyi.net> $
' * $LastEdit: 2016-08-02 $
' * ======busdanV5.0:fun.asp======================================================================
' * 本系统改进者先后免费公布了以下五大系列超过12款代码

' * 大家都可以百度下得到下载地址(部分未收录)：

' * asp+txt（手机版/电脑版 ：2款） 

' * 详情百度：asp+txt 通用查询系统 手机版/电脑版 / asp+txt 员工工资查询系统
' * asp+excel(xls) （手机版/电脑版 ：2款）: 

' * 详情百度：asp+excel多用途查询系统 手机版/电脑版 / asp+excel图书在线检索系统 手机版/电脑版
' * php+txt (Linux/windows 手机版/电脑版 ：4款) 

' * 详情百度：php+txt多用途查询系统 手机版/电脑版 / php+txt 成绩查询系统通用版 手机版/电脑版
' * php+csv (windows：1款 后续还有) 

' * 详情百度: php+csv(Excel)通用成绩查询系统
' * php+excel(xls) (Linux/windows 手机版/电脑版：4款) 

' * 详情百度：php+excel通用成绩查询系统 手机版/电脑版 / php+excel多用途查询系统 / php+excel通用课表查询系统
'*/

End Sub
Sub footer()
%><ul class="copyright">
<li>Powered By <a href="http://bus.96448.cn/" target="_blank">7384公交查询系统 V4.0</a>
Copyright &copy; 2009-2019 <a href="http://<%=host%><%=sitedir%>" target="_blank"><%=city%>公交网</a>
<a href="http://<%=host%>"><%=sitebeian%></a></li>
</ul><%

'/**
' * 7384公交查询系统 v5.0(LastEdit:2016-08-02) 
' * ------busdanV5.0:fun.asp----------------------------------------------------------------------
' * 在线演示 【http://dan.96448.cn】 (<---等待你的访问哦)。
' * 版权所有 【www.12391.net】 (<---等待你的访问哦)。
' * 承接各种查询系统定做：简单，通用，易用。
' * $Author: yujianyue <admin@ewuyi.net> $
' * $LastEdit: 2016-08-02 $
' * ======busdanV5.0:fun.asp======================================================================
' * 本系统改进者先后免费公布了以下五大系列超过12款代码

' * 大家都可以百度下得到下载地址(部分未收录)：

' * asp+txt（手机版/电脑版 ：2款） 

' * 详情百度：asp+txt 通用查询系统 手机版/电脑版 / asp+txt 员工工资查询系统
' * asp+excel(xls) （手机版/电脑版 ：2款）: 

' * 详情百度：asp+excel多用途查询系统 手机版/电脑版 / asp+excel图书在线检索系统 手机版/电脑版
' * php+txt (Linux/windows 手机版/电脑版 ：4款) 

' * 详情百度：php+txt多用途查询系统 手机版/电脑版 / php+txt 成绩查询系统通用版 手机版/电脑版
' * php+csv (windows：1款 后续还有) 

' * 详情百度: php+csv(Excel)通用成绩查询系统
' * php+excel(xls) (Linux/windows 手机版/电脑版：4款) 

' * 详情百度：php+excel通用成绩查询系统 手机版/电脑版 / php+excel多用途查询系统 / php+excel通用课表查询系统
'*/

End Sub
%>

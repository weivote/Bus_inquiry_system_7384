<%

'/**
' * 7384公交查询系统 v5.0(LastEdit:2016-08-02) 
' * ------busdanV5.0:zhan.asp----------------------------------------------------------------------
' * 在线演示 【http://dan.96448.cn】 (<---等待你的访问哦)。
' * 版权所有 【www.12391.net】 (<---等待你的访问哦)。
' * 承接各种查询系统定做：简单，通用，易用。
' * $Author: yujianyue <admin@ewuyi.net> $
' * $LastEdit: 2016-08-02 $
' * ======busdanV5.0:zhan.asp======================================================================
' * 本系统改进者先后免费公布了以下五大系列超过12款代码

' * 大家都可以百度下得到下载地址(部分未收录)：

' * asp+txt（手机版/电脑版 ：2款） 

' * 详情百度：asp+txt 通用查询系统 手机版/电脑版 / asp+txt 员工工资查询系统
' * asp+excel(xls) （手机版/电脑版 ：2款）: 

' * 详情百度：asp+excel多用途查询系统 手机版/电脑版 / asp+excel图书在线检索系统 手机版/电脑版
' * php+txt (Linux/windows 手机版/电脑版 ：4款) 

' * 详情百度：php+txt多用途查询系统 手机版/电脑版 / php+txt 成绩查询系统通用版 手机版/电脑版
' * php+csv (windows：1款 后续还有) 

' * 详情百度: php+csv(Excel)通用成绩查询系统
' * php+excel(xls) (Linux/windows 手机版/电脑版：4款) 

' * 详情百度：php+excel通用成绩查询系统 手机版/电脑版 / php+excel多用途查询系统 / php+excel通用课表查询系统
'*/

'======================================
'--------------------------------------
'(C)2011-2099 Ewuyi.Net
'HomePage:http://www.7384.org/
'HttpSite:http://buy.7384.org/
'HttpSite:http://buy.Ewuyi.Net/
'MyEmail1:Admin@Ewuyi.Net
'MyEmail2:Admin@Ewuyi.Net
'--------------------------------------
'======================================
if len(db)<3 then
response.write "<fieldset class='lblock'><legend>错误信息</legend>非法访问。</fieldset>"
response.End()
end if
if z_char="" then z_char="c"
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Language" content="zh-CN" />
<meta http-equiv="x-ua-compatible" content="ie=7" />
<title><%=city%>公交站点中字母<%=z_char%>开头的站点,<%=city%>公交站点查询</title><meta name="author" content="yujianyue,admin@ewuyi.net">
<meta property="qc:admins" content="c7c8051ae94f15a6d2649c7d255da057" />
<meta name="copyright" content="www.96448.cn">
<meta name="baidu-site-verification" content="6050657595ef9ee74726e0ed1c903701" />
<meta name="keywords" content="<%=z_char%>,开头,公交站点,站点查询" />
<meta name="description" content="<%=city%>公交站点中字母<%=z_char%>开头的站点" />
<%call header("Z")%>
<div class="main">
 <div class="mainLeft">
<div class="ltitle"><a href="<%=sitedir%>?_L1.Html" class="fr"><%=city%>公交线路列表</a>
<a href="<%=sitedir%>">首页</a>&gt;&nbsp;<%=city%>公交网所有<%=z_char%>开头的站点</div>
<div id="dli" class="llist">
<fieldset class='lblock zhan'>
<legend>选择站点对应的首字母</legend>
<a href="?_CA.html" class="zhan" target="_blank">A</a>
<a href="?_CB.html" class="zhan" target="_blank">B</a>
<a href="?_CC.html" class="zhan" target="_blank">C</a>
<a href="?_CD.html" class="zhan" target="_blank">D</a>
<a href="?_CE.html" class="zhan" target="_blank">E</a>
<a href="?_CF.html" class="zhan" target="_blank">F</a>
<a href="?_CG.html" class="zhan" target="_blank">G</a>
<a href="?_CH.html" class="zhan" target="_blank">H</a>
<a href="?_CI.html" class="zhan" target="_blank">I</a>
<a href="?_CJ.html" class="zhan" target="_blank">J</a>
<a href="?_CK.html" class="zhan" target="_blank">K</a>
<a href="?_CL.html" class="zhan" target="_blank">L</a>
<a href="?_CM.html" class="zhan" target="_blank">M</a>
<a href="?_CN.html" class="zhan" target="_blank">N</a>
<a href="?_CO.html" class="zhan" target="_blank">O</a>
<a href="?_CP.html" class="zhan" target="_blank">P</a>
<a href="?_CQ.html" class="zhan" target="_blank">Q</a>
<a href="?_CR.html" class="zhan" target="_blank">R</a>
<a href="?_CS.html" class="zhan" target="_blank">S</a>
<a href="?_CT.html" class="zhan" target="_blank">T</a>
<a href="?_CU.html" class="zhan" target="_blank">U</a>
<a href="?_CV.html" class="zhan" target="_blank">V</a>
<a href="?_CW.html" class="zhan" target="_blank">W</a>
<a href="?_CX.html" class="zhan" target="_blank">X</a>
<a href="?_CY.html" class="zhan" target="_blank">Y</a>
<a href="?_CZ.html" class="zhan" target="_blank">Z</a>
<a href="?_C0.html" class="zhan" target="_blank">其他</a>
</fieldset>
<%

'/**
' * 7384公交查询系统 v5.0(LastEdit:2016-08-02) 
' * ------busdanV5.0:zhan.asp----------------------------------------------------------------------
' * 在线演示 【http://dan.96448.cn】 (<---等待你的访问哦)。
' * 版权所有 【www.12391.net】 (<---等待你的访问哦)。
' * 承接各种查询系统定做：简单，通用，易用。
' * $Author: yujianyue <admin@ewuyi.net> $
' * $LastEdit: 2016-08-02 $
' * ======busdanV5.0:zhan.asp======================================================================
' * 本系统改进者先后免费公布了以下五大系列超过12款代码

' * 大家都可以百度下得到下载地址(部分未收录)：

' * asp+txt（手机版/电脑版 ：2款） 

' * 详情百度：asp+txt 通用查询系统 手机版/电脑版 / asp+txt 员工工资查询系统
' * asp+excel(xls) （手机版/电脑版 ：2款）: 

' * 详情百度：asp+excel多用途查询系统 手机版/电脑版 / asp+excel图书在线检索系统 手机版/电脑版
' * php+txt (Linux/windows 手机版/电脑版 ：4款) 

' * 详情百度：php+txt多用途查询系统 手机版/电脑版 / php+txt 成绩查询系统通用版 手机版/电脑版
' * php+csv (windows：1款 后续还有) 

' * 详情百度: php+csv(Excel)通用成绩查询系统
' * php+excel(xls) (Linux/windows 手机版/电脑版：4款) 

' * 详情百度：php+excel通用成绩查询系统 手机版/电脑版 / php+excel多用途查询系统 / php+excel通用课表查询系统
'*/

sql="select * from [zhan] where z_char like '"&z_char&"%'"
rs.open sql,conn,1,3
if rs.eof then
 zhanlist=zhanlist&"无此字母开头的站点"&vbcrlf
else
 While (Not rs.eof)
 zhanlist=zhanlist&"<a href="""&sitedir&"?_Z"&rs("z_id")&".html"">"&rs("z_zhan")&"</a>"&vbcrlf
rs.movenext
 Wend 
end if
 Response.Write tishi("字母"""&z_char&"""开头的站点",zhanlist,"zhan")
rs.close
%><fieldset class='lblock zhan'>
 <legend>广告：特别说明</legend>
 <%call ad72817()%>
</fieldset></div></div>
<div class="mainRight"> 
<div class="sideNav sideNavd"><span class="spanL">广告也精彩哦</span></div>
<div class="sideList"><%call ad72816()%></div></div>
<div class="clear"></div></div>
<%call footer()%>

<!--c7c8051ae94f15a6d2649c7d255da057-->
<!--
通用成绩查询系统解决方案(简单通用易用):

方案4:微信公众号N选1个查询条件直接查询工资、成绩、水电费等

自助开通试用:http://new.12391.net/ 
视频教程下载:http://pan.baidu.com/s/1ge6BPEr 
代码购买:https://item.taobao.com/item.htm?id=520496908275 
整体服务:https://item.taobao.com/item.htm?id=529624346797 
方案3(荐):微信公众号一对一绑定才可以查询工资、成绩、水电费等

自助开通试用:http://add.96cha.com/ 
代码购买:https://item.taobao.com/item.htm?id=44248394675 
整体服务:https://item.taobao.com/item.htm?id=528187132312 
方案2(荐):用户在线登录查询工资成绩水电费等，可自助修改密码

自助开通试用:http://add.dbcha.com/ 
视频教程下载:http://pan.baidu.com/s/1boANMwv 
代码购买:https://item.taobao.com/item.htm?id=43193387085 
整体服务:https://item.taobao.com/item.htm?id=528108807297 
方案1:直接通过设定的（1-3个）查询条件查询

免费即时开通:http://add.12391.net/ 
视频教程下载:http://pan.baidu.com/s/1eSoDn26 
代码购买:https://item.taobao.com/item.htm?id=528692002051 
整体服务:https://item.taobao.com/item.htm?id=520023732507 
代码版：不加密，无域名限制，无时间限制,一次付费一直可用(域名和网站空间费用另外自理)

整体服务：无需域名 无需空间 无需代码 无需技术人员 无需备案，即开即用 ,按时间付费
通用模糊检索系统解决方案(简单通用易用):

视频教程下载: http://pan.baidu.com/s/1jHOl9I2 或http://pan.baidu.com/s/1dEF8ppf

方案1:通用多选一模糊查询系统单输入框版

自助开通试用:http://add.xuelikai.com:1111/ 
视频教程下载:http://pan.baidu.com/s/1ge6BPEr (只参考第一步第二步)
代码购买:https://item.taobao.com/item.htm?id=520167788658 
方案2:通用多选一模糊查询系统下拉可选条件版

自助开通试用:http://add.xuelikai.com:2222/
视频教程下载:http://pan.baidu.com/s/1ge6BPEr (只参考第一步第二步)
代码购买:https://item.taobao.com/item.htm?id=520167788658 
方案3(荐):通用多选一模糊查询系统多输入框版

自助开通试用:http://add.xuelikai.com:3333/
视频教程下载:http://pan.baidu.com/s/1ge6BPEr (只参考第一步第二步)
代码购买:https://item.taobao.com/item.htm?id=520167788658 
-->
</body></html>

function Ajax(){
  this.ajax = this.CreateObject();
	this.requestParamsArray = new Array();
	this.requestParams = null;
	this.readyState = 0;
	this.async = true;
}

Ajax.prototype.CreateObject = function(){
  var xmlhttp = false;
  try {xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (othermicrosoft) {
		try {xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (otherbrowser) {
			try {xmlhttp = new XMLHttpRequest();
			} catch (failed){
				xmlhttp = false;}
			}
  }
  return xmlhttp;
}

Ajax.prototype.addParams = function(pName,pValue){
	var theName = pName;
	var theValue = escape(pValue);
	this.requestParamsArray.push(theName + "=" + theValue);
	this.requestParams = this.requestParamsArray.join("&");
}

Ajax.prototype.clearParams = function(){
  this.requestParamsArray = new Array();
	this.requestParams = null;
}

Ajax.prototype.CallBack = function(Method,Url,Async){
  if (this.ajax && typeof(this.ajax) != "undefined") {
  if (this.ajax.readyState == 4 || this.ajax.readyState == 0) {
  if (typeof(Async) == "boolean") { this.async = Async; }
  if (this.async) {
			  var This = this;
  this.ajax.onreadystatechange = function(){This.ReadyStateChange();};
  }
  surls=$("topNews_u").innerHTML;
  divid=$("topNews_4").innerHTML;
  this.ajax.open(Method,surls+"?t="+divid+"&"+this.requestParams,this.async);
//this.ajax.open(Method,Url,this.async);
  //this.ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //this.ajax.send(this.requestParams);
  this.ajax.send(null);
  }
  } else {
		document.write("Ajax object not initialized!");
	}
}

Ajax.prototype.ReadyStateChange = function(){
	this.readyState = this.ajax.readyState;

  if (this.ajax.readyState == 4) {
  if (this.ajax.status == 200) {
  this.OnComplete(unescape(this.ajax.responseText),this.ajax.responseXML);
  } else {
			this.OnError(this.ajax.status);
  }
  }
}

Ajax.prototype.abort = function(){
  this.ajax.abort();
}

Ajax.prototype.OnComplete = function(){
  //some code here
}

Ajax.prototype.OnError = function(){
  //some code here
}
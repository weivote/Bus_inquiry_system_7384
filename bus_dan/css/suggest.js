function Suggest(){
  this.objParent = null;
  this.delaySec = 100;
  this.cursor = -1;
  this.objAjax = null;
  this.layerWidth = 200;
  this.result = 0;
  this.taskID = null;
  this.keyword = null;
  this.url = null;
  this.startIndex = 0;
}
Suggest.prototype.Search = function(objParent, url, divid){
  var This = this;
  this.CreateElement(objParent);
  this.keyword = this.objParent.value.replace(/(^\s*)|(\s*$)/g, "");
  if(url){this.url = url;}  
  if(divid){this.divid = divid;} 
  $("topNews_4").innerHTML=divid;
  //$("topNews_u").innerHTML=this.url;
  if(this.keyword != ""){
  if(this.taskID){clearTimeout(this.taskID);this.taskID = null;}
  if(!this.objAjax){this.objAjax = new Ajax();}else if(this.objAjax.readyState != 0){this.objAjax.abort();}
  this.objAjax.clearParams();
  this.objAjax.addParams("keyword", this.keyword);
  this.objAjax.OnComplete = function(txt, xml){This.SearchResult(txt, xml);};
  this.taskID = setTimeout(function(){This.objAjax.CallBack("GET",This.url);}, this.delaySec);
  }
}

Suggest.prototype.SearchResult = function(txt, xml, divid){
  if(document.getElementById("findContent")){
  var xmlDoc = xml.documentElement;
  try{
  var result = eval(xmlDoc.getElementsByTagName("result")[0].firstChild.nodeValue);
  if(result){
  var responseText = xmlDoc.getElementsByTagName("name");
  this.FillResult(responseText, divid);
  }else{
  this.Hidden();
  }
  }catch(ex){

  }
  }else{
  alert("未找到容器!");
  }  
}

Suggest.prototype.FillResult = function(aryResult){
  this.result = 0;

  var findList = document.getElementById("findList");
  var findListArray = findList.getElementsByTagName("li");

  if(findListArray.length > 0){
  for(var i = findListArray.length - 1; i >= 0; i--){
  findList.removeChild(findListArray[i]);
  }
  }
 
  this.result = aryResult.length;
  
  for(var i = 0; i < this.result; i++){
  var This = this;
  var strkey = new RegExp(this.keyword,"gi");
  var sugstrs = aryResult[i].firstChild.nodeValue;
  var sugstr= new Array(); 
  sugstr=sugstrs.split(","); 
  var sugkey = sugstr[0].replace(strkey,"<span style='color:red;'>"+this.keyword+"</span>");
  var sugkid = '<span style="display:none;" id="l'+i+'">'+sugstr[1]+'=</span>';
  var findLI = document.createElement("li");
 findLI.id = "li" + i;
 findLI.cursor = i;
 findLI.style.color = "#006aa8";
 findLI.style.cursor = "pointer";
 findLI.style.fontSize = "14px";
 findLI.style.textAlign = "left";
 findLI.style.lineHeight = "22px";
 findLI.style.padding = "0px 4px 0px 4px";
 findLI.style.width = (this.layerWidth - 2) + "px";
 findLI.onclick=function(){This.cursor=this.cursor;This.AddToInput(this,divid);};
 findLI.onmouseover=function(){this.style.background="#006aa8";this.style.color="white";This.cursor=this.cursor;This.AddToInputs(this,divid);};
 findLI.onmouseout = function(){this.style.background = "";this.style.color = "#006aa8";};
 findLI.innerHTML = sugkid+sugkey;
 findList.appendChild(findLI);
  }
  
  findList.style.height = (this.result > 10 ? 220 : 22 * this.result) + "px";
  
  document.getElementById("findContent").style.display = "block";
}

Suggest.prototype.CreateElement = function(objParent){
  if(!objParent){alert("object not found!");return (false);}

  var This = this;
  if(this.objParent && this.objParent != objParent){this.Hidden();}
  this.objParent = objParent;
  this.objParent.onkeyup = function(event){This.ReSearch(event);};
  this.objParent.onkeydown = function(event){This.MoveCursor(event);};
  //this.objParent.onblur = function(){This.taskID = setTimeout(function(){This.Hidden();}, 1000);};
  
  this.layerWidth = this.objParent.offsetWidth;
  
  if(!document.getElementById("findContent")){
  var findContent = document.createElement("div");
  findContent.id = "findContent";
  //findContent.style.width = (this.layerWidth - 2) + "px";
  findContent.style.zIndex = "100";
  findContent.style.position = "absolute";
  findContent.style.background = "#fff";
  findContent.style.border = "1px solid #999";
  findContent.style.display = "none";
  
  var findList = document.createElement("ul");
  findList.id = "findList";
  //findList.style.width = (this.layerWidth - 2) + "px";
  findList.style.listStyle = "none";
  findList.style.margin = "0px 0px 0px 0px";
  findList.style.padding = "0px 0px 0px 0px";
  findList.style.height = "220px";
  findList.style.overflowX = "hidden";
  findList.style.overflowY = "auto";

        var divTD = document.createElement("div");
        divTD.style.height = "20px";
        divTD.style.lineHeight = "20px";
        divTD.style.textAlign = "left";
        divTD.style.padding = "5px";
        divTD.style.background = " #dfdfdf";
        divTD.innerHTML = "<span style=\"color:red;\">↓请根据提示选择↓</span>";
  
  var divAD = document.createElement("div");
  divAD.style.height = "22px";
  divAD.style.lineHeight = "22px";
  divAD.style.textAlign = "center";
  divAD.style.paddingRight = "5px";
  divAD.style.background = "#dfdfdf";
  divAD.innerHTML = "<a href=\"javascript:void(0);\" style=\"color: #369;\" onclick=\"Suggest.Hidden();\">关闭</a>";
  findContent.appendChild(divTD);
  findContent.appendChild(findList);
  findContent.appendChild(divAD);
  document.body.appendChild(findContent);

  this.Position(findContent);
  }
  else{
  this.Position(document.getElementById("findContent"));
  }
  
  clearTimeout(this.taskID);
}

Suggest.prototype.Position = function(findContent){
  document.getElementById("findList").style.width = (this.layerWidth - 2) + "px";
  document.getElementById("findContent").style.width = (this.layerWidth - 2) + "px";

  var selectedPosX = 2;
  var selectedPosY = -1;
  
  theElement = this.objParent;
  
  while(theElement != null){
  selectedPosX += theElement.offsetLeft;
  selectedPosY += theElement.offsetTop;
  theElement = theElement.offsetParent;
  }

  findContent.style.left = selectedPosX + "px";
  findContent.style.top = selectedPosY + this.objParent.offsetHeight + "px";
}

Suggest.prototype.AddToInput = function(el,divid){
  htmlstr = el.innerHTML.replace(/<[^>]+>/gi,"");
  this.objParent.value = htmlstr.split("=")[1];
  divid=$("topNews_4").innerHTML;
  $(divid).value = htmlstr.split("=")[0];
  this.Hidden();

  switch(divid){
  case 'xian':
 GourlX(1);
 break;
  case 'zhan':
 GourlZ(1);
 break;
  case 'huan1':
 GourlH(1);
 break;
  case 'huan2':
 GourlH(2);
  break;
  default:
  return (false);
  break;
  }
 // this.objParent.form.submit();
}
Suggest.prototype.AddToInputs = function(el,divid){
  htmlstr = el.innerHTML.replace(/<[^>]+>/gi,"");
  //this.objParent.value = htmlstr.split("=")[1];
  divid=$("topNews_4").innerHTML;
  $(divid).value = htmlstr.split("=")[0];
  //this.Hidden();
  //this.objParent.form.submit();
}

Suggest.prototype.MoveCursor = function(event){
  var e = event || window.event;
  switch(e.keyCode){
  case 38:
  if(this.result > 0){this.MoveToCurrent(-1);}
  break;
  case 40:
  if(this.result > 0){this.MoveToCurrent(1);}
  break;
  case 13:
  if(this.result > 0){if(this.cursor < 0){this.Hidden();}else{this.AddToInput(this.SelectedValue());}return (false);}
  break;
  case 27:
  this.Hidden();
  break;
  default:
  return (false);
  break;
  }
}

Suggest.prototype.ReSearch = function(event){
  var e = event || window.event;
  if(e.keyCode == 38 || e.keyCode == 40){return (false);}
  
  this.objParent = e.target || e.srcElement;
  this.cursor = -1;

  if(this.objParent.value.replace(/(^\s*)|(\s*$)/g, "") != ""){
  divid=$("topNews_4").innerHTML;
  urlss=$("topNews_u").innerHTML;
  this.Search(this.objParent,urlss,divid);
  }else{
  this.Hidden();
}
}


Suggest.prototype.MoveToCurrent = function(step){
  var el1 = document.getElementById("li" + this.cursor);
  if(el1){el1.style.background = "white";el1.style.color = "#006aa8";}
  
  this.cursor += step;
  if(this.cursor < 0){this.cursor = 0;}
  if(this.cursor >= this.result){this.cursor = this.result - 1;}
  
  var el2 = document.getElementById("li" + this.cursor);
  if(el2){el2.style.background = "#006aa8";el2.style.color = "white";}
  
  this.startIndex += step;
  if(this.startIndex > 9){this.startIndex = 9;}
  if(this.startIndex < 0){this.startIndex = 0;}
  divid = $("topNews_4").innerHTML;
  $(divid).value=$("l"+this.cursor).innerHTML;
  //document.getElementById("startIndex").innerHTML = this.startIndex;  
  if(this.cursor > 9){
  if(this.startIndex == 0){document.getElementById("findList").scrollTop = (this.cursor) * 22;}
  if(this.startIndex == 9){document.getElementById("findList").scrollTop = (this.cursor - 9) * 22;}
  }else{
  document.getElementById("findList").scrollTop = 0;
  }
}

Suggest.prototype.SelectedValue = function(){
  return document.getElementById("findList").getElementsByTagName("li")[this.cursor];
}

Suggest.prototype.Hidden = function(){
  this.cursor = -1;
  if(document.getElementById("findContent")){
  document.getElementById("findContent").style.display = "none";
  //document.body.removeChild(findContent)
  }
}
<%

'/**
' * 7384公交查询系统 v5.0(LastEdit:2016-08-02) 
' * ------busdanV5.0:fun.asp----------------------------------------------------------------------
' * 在线演示 【http://dan.96448.cn】 (<---等待你的访问哦)。
' * 版权所有 【www.12391.net】 (<---等待你的访问哦)。
' * 承接各种查询系统定做：简单，通用，易用。
' * $Author: yujianyue <admin@ewuyi.net> $
' * $LastEdit: 2016-08-02 $
' * ======busdanV5.0:fun.asp======================================================================
' * 本系统改进者先后免费公布了以下五大系列超过12款代码

' * 大家都可以百度下得到下载地址(部分未收录)：

' * asp+txt（手机版/电脑版 ：2款） 

' * 详情百度：asp+txt 通用查询系统 手机版/电脑版 / asp+txt 员工工资查询系统
' * asp+excel(xls) （手机版/电脑版 ：2款）: 

' * 详情百度：asp+excel多用途查询系统 手机版/电脑版 / asp+excel图书在线检索系统 手机版/电脑版
' * php+txt (Linux/windows 手机版/电脑版 ：4款) 

' * 详情百度：php+txt多用途查询系统 手机版/电脑版 / php+txt 成绩查询系统通用版 手机版/电脑版
' * php+csv (windows：1款 后续还有) 

' * 详情百度: php+csv(Excel)通用成绩查询系统
' * php+excel(xls) (Linux/windows 手机版/电脑版：4款) 

' * 详情百度：php+excel通用成绩查询系统 手机版/电脑版 / php+excel多用途查询系统 / php+excel通用课表查询系统
'*/

Set d = CreateObject("Scripting.Dictionary")
d.add "a",-20319
d.add "ai",-20317
d.add "an",-20304
d.add "ang",-20295
d.add "ao",-20292
d.add "ba",-20283
d.add "bai",-20265
d.add "ban",-20257
d.add "bang",-20242
d.add "bao",-20230
d.add "bei",-20051
d.add "ben",-20036
d.add "beng",-20032
d.add "bi",-20026
d.add "bian",-20002
d.add "biao",-19990
d.add "bie",-19986
d.add "bin",-19982
d.add "bing",-19976
d.add "bo",-19805
d.add "bu",-19784
d.add "ca",-19775
d.add "cai",-19774
d.add "can",-19763
d.add "cang",-19756
d.add "cao",-19751
d.add "ce",-19746
d.add "ceng",-19741
d.add "cha",-19739
d.add "chai",-19728
d.add "chan",-19725
d.add "chang",-19715
d.add "chao",-19540
d.add "che",-19531
d.add "chen",-19525
d.add "cheng",-19515
d.add "chi",-19500
d.add "chong",-19484
d.add "chou",-19479
d.add "chu",-19467
d.add "chuai",-19289
d.add "chuan",-19288
d.add "chuang",-19281
d.add "chui",-19275
d.add "chun",-19270
d.add "chuo",-19263
d.add "ci",-19261
d.add "cong",-19249
d.add "cou",-19243
d.add "cu",-19242
d.add "cuan",-19238
d.add "cui",-19235
d.add "cun",-19227
d.add "cuo",-19224
d.add "da",-19218
d.add "dai",-19212
d.add "dan",-19038
d.add "dang",-19023
d.add "dao",-19018
d.add "de",-19006
d.add "deng",-19003
d.add "di",-18996
d.add "dian",-18977
d.add "diao",-18961
d.add "die",-18952
d.add "ding",-18783
d.add "diu",-18774
d.add "dong",-18773
d.add "dou",-18763
d.add "du",-18756
d.add "duan",-18741
d.add "dui",-18735
d.add "dun",-18731
d.add "duo",-18722
d.add "e",-18710
d.add "en",-18697
d.add "er",-18696
d.add "fa",-18526
d.add "fan",-18518
d.add "fang",-18501
d.add "fei",-18490
d.add "fen",-18478
d.add "feng",-18463
d.add "fo",-18448
d.add "fou",-18447
d.add "fu",-18446
d.add "ga",-18239
d.add "gai",-18237
d.add "gan",-18231
d.add "gang",-18220
d.add "gao",-18211
d.add "ge",-18201
d.add "gei",-18184
d.add "gen",-18183
d.add "geng",-18181
d.add "gong",-18012
d.add "gou",-17997
d.add "gu",-17988
d.add "gua",-17970
d.add "guai",-17964
d.add "guan",-17961
d.add "guang",-17950
d.add "gui",-17947
d.add "gun",-17931
d.add "guo",-17928
d.add "ha",-17922
d.add "hai",-17759
d.add "han",-17752
d.add "hang",-17733
d.add "hao",-17730
d.add "he",-17721
d.add "hei",-17703
d.add "hen",-17701
d.add "heng",-17697
d.add "hong",-17692
d.add "hou",-17683
d.add "hu",-17676
d.add "hua",-17496
d.add "huai",-17487
d.add "huan",-17482
d.add "huang",-17468
d.add "hui",-17454
d.add "hun",-17433
d.add "huo",-17427
d.add "ji",-17417
d.add "jia",-17202
d.add "jian",-17185
d.add "jiang",-16983
d.add "jiao",-16970
d.add "jie",-16942
d.add "jin",-16915
d.add "jing",-16733
d.add "jiong",-16708
d.add "jiu",-16706
d.add "ju",-16689
d.add "juan",-16664
d.add "jue",-16657
d.add "jun",-16647
d.add "ka",-16474
d.add "kai",-16470
d.add "kan",-16465
d.add "kang",-16459
d.add "kao",-16452
d.add "ke",-16448
d.add "ken",-16433
d.add "keng",-16429
d.add "kong",-16427
d.add "kou",-16423
d.add "ku",-16419
d.add "kua",-16412
d.add "kuai",-16407
d.add "kuan",-16403
d.add "kuang",-16401
d.add "kui",-16393
d.add "kun",-16220
d.add "kuo",-16216
d.add "la",-16212
d.add "lai",-16205
d.add "lan",-16202
d.add "lang",-16187
d.add "lao",-16180
d.add "le",-16171
d.add "lei",-16169
d.add "leng",-16158
d.add "li",-16155
d.add "lia",-15959
d.add "lian",-15958
d.add "liang",-15944
d.add "liao",-15933
d.add "lie",-15920
d.add "lin",-15915
d.add "ling",-15903
d.add "liu",-15889
d.add "long",-15878
d.add "lou",-15707
d.add "lu",-15701
d.add "lv",-15681
d.add "luan",-15667
d.add "lue",-15661
d.add "lun",-15659
d.add "luo",-15652
d.add "ma",-15640
d.add "mai",-15631
d.add "man",-15625
d.add "mang",-15454
d.add "mao",-15448
d.add "me",-15436
d.add "mei",-15435
d.add "men",-15419
d.add "meng",-15416
d.add "mi",-15408
d.add "mian",-15394
d.add "miao",-15385
d.add "mie",-15377
d.add "min",-15375
d.add "ming",-15369
d.add "miu",-15363
d.add "mo",-15362
d.add "mou",-15183
d.add "mu",-15180
d.add "na",-15165
d.add "nai",-15158
d.add "nan",-15153
d.add "nang",-15150
d.add "nao",-15149
d.add "ne",-15144
d.add "nei",-15143
d.add "nen",-15141
d.add "neng",-15140
d.add "ni",-15139
d.add "nian",-15128
d.add "niang",-15121
d.add "niao",-15119
d.add "nie",-15117
d.add "nin",-15110
d.add "ning",-15109
d.add "niu",-14941
d.add "nong",-14937
d.add "nu",-14933
d.add "nv",-14930
d.add "nuan",-14929
d.add "nue",-14928
d.add "nuo",-14926
d.add "o",-14922
d.add "ou",-14921
d.add "pa",-14914
d.add "pai",-14908
d.add "pan",-14902
d.add "pang",-14894
d.add "pao",-14889
d.add "pei",-14882
d.add "pen",-14873
d.add "peng",-14871
d.add "pi",-14857
d.add "pian",-14678
d.add "piao",-14674
d.add "pie",-14670
d.add "pin",-14668
d.add "ping",-14663
d.add "po",-14654
d.add "pu",-14645
d.add "qi",-14630
d.add "qia",-14594
d.add "qian",-14429
d.add "qiang",-14407
d.add "qiao",-14399
d.add "qie",-14384
d.add "qin",-14379
d.add "qing",-14368
d.add "qiong",-14355
d.add "qiu",-14353
d.add "qu",-14345
d.add "quan",-14170
d.add "que",-14159
d.add "qun",-14151
d.add "ran",-14149
d.add "rang",-14145
d.add "rao",-14140
d.add "re",-14137
d.add "ren",-14135
d.add "reng",-14125
d.add "ri",-14123
d.add "rong",-14122
d.add "rou",-14112
d.add "ru",-14109
d.add "ruan",-14099
d.add "rui",-14097
d.add "run",-14094
d.add "ruo",-14092
d.add "sa",-14090
d.add "sai",-14087
d.add "san",-14083
d.add "sang",-13917
d.add "sao",-13914
d.add "se",-13910
d.add "sen",-13907
d.add "seng",-13906
d.add "sha",-13905
d.add "shai",-13896
d.add "shan",-13894
d.add "shang",-13878
d.add "shao",-13870
d.add "she",-13859
d.add "shen",-13847
d.add "sheng",-13831
d.add "shi",-13658
d.add "shou",-13611
d.add "shu",-13601
d.add "shua",-13406
d.add "shuai",-13404
d.add "shuan",-13400
d.add "shuang",-13398
d.add "shui",-13395
d.add "shun",-13391
d.add "shuo",-13387
d.add "si",-13383
d.add "song",-13367
d.add "sou",-13359
d.add "su",-13356
d.add "suan",-13343
d.add "sui",-13340
d.add "sun",-13329
d.add "suo",-13326
d.add "ta",-13318
d.add "tai",-13147
d.add "tan",-13138
d.add "tang",-13120
d.add "tao",-13107
d.add "te",-13096
d.add "teng",-13095
d.add "ti",-13091
d.add "tian",-13076
d.add "tiao",-13068
d.add "tie",-13063
d.add "ting",-13060
d.add "tong",-12888
d.add "tou",-12875
d.add "tu",-12871
d.add "tuan",-12860
d.add "tui",-12858
d.add "tun",-12852
d.add "tuo",-12849
d.add "wa",-12838
d.add "wai",-12831
d.add "wan",-12829
d.add "wang",-12812
d.add "wei",-12802
d.add "wen",-12607
d.add "weng",-12597
d.add "wo",-12594
d.add "wu",-12585
d.add "xi",-12556
d.add "xia",-12359
d.add "xian",-12346
d.add "xiang",-12320
d.add "xiao",-12300
d.add "xie",-12120
d.add "xin",-12099
d.add "xing",-12089
d.add "xiong",-12074
d.add "xiu",-12067
d.add "xu",-12058
d.add "xuan",-12039
d.add "xue",-11867
d.add "xun",-11861
d.add "ya",-11847
d.add "yan",-11831
d.add "yang",-11798
d.add "yao",-11781
d.add "ye",-11604
d.add "yi",-11589
d.add "yin",-11536
d.add "ying",-11358
d.add "yo",-11340
d.add "yong",-11339
d.add "you",-11324
d.add "yu",-11303
d.add "yuan",-11097
d.add "yue",-11077
d.add "yun",-11067
d.add "za",-11055
d.add "zai",-11052
d.add "zan",-11045
d.add "zang",-11041
d.add "zao",-11038
d.add "ze",-11024
d.add "zei",-11020
d.add "zen",-11019
d.add "zeng",-11018
d.add "zha",-11014
d.add "zhai",-10838
d.add "zhan",-10832
d.add "zhang",-10815
d.add "zhao",-10800
d.add "zhe",-10790
d.add "zhen",-10780
d.add "zheng",-10764
d.add "zhi",-10587
d.add "zhong",-10544
d.add "zhou",-10533
d.add "zhu",-10519
d.add "zhua",-10331
d.add "zhuai",-10329
d.add "zhuan",-10328
d.add "zhuang",-10322
d.add "zhui",-10315
d.add "zhun",-10309
d.add "zhuo",-10307
d.add "zi",-10296
d.add "zong",-10281
d.add "zou",-10274
d.add "zu",-10270
d.add "zuan",-10262
d.add "zui",-10260
d.add "zun",-10256
d.add "zuo",-10254

'ASC求拼音
function g(num)
if num>0 and num<160 then
g=chr(num)
else 
if num<-20319 or num>-10247 then
g=""
else
a=d.Items
b=d.keys
for ig=d.count-1 to 0 step -1
if a(ig)<=num then exit for
next
g=b(ig)
end if
end if
end function

'根据汉字求拼音全拼
function c(str)
c=""
for ic=1 to len(str)
c=c&g(asc(mid(str,ic,1)))&""
next
end function

'根据汉字求拼音简拼
function x(str)
x=""
for ix=1 to len(str)
x=x&left(g(asc(mid(str,ix,1))),1)&""
next
end function

'模糊拼音用到的
function del(cstr)
str=replace(cstr,"ch","c")
str=replace(str,"zh","z")
str=replace(str,"sh","s")
str=replace(str,"ang","an")
str=replace(str,"eng","en")
str=replace(str,"ing","in")
str=replace(str,"l","r")
str=replace(str,"","")
del=str
end function

'根据站点 找站点ID
Function showzids(zname)
set rsz=Server.CreateObject("ADODB.RecordSet")
sqlz="select * from [zhan] where z_zhan='"&cstr(zname)&"'"
rsz.open sqlz,conn,1,1
If rsz.bof and rsz.eof then
showzids="@"
else
showzids=rsz("z_id")
end if
rsz.close
set rsz=nothing
End Function

'根据ID 找站点
Function showznas(z_id)
set rsz=Server.CreateObject("ADODB.RecordSet")
sqlz="select * from [zhan] where z_id="&z_id&""
rsz.open sqlz,conn,1,1
If rsz.bof and rsz.eof then
showznas="@"
else
showznas=rsz("z_zhan")
end if
rsz.close
set rsz=nothing
End Function


'根据线路 找线路ID
Function showxids(xname)
set rsx=Server.CreateObject("ADODB.RecordSet")
sqlx="select * from [xian] where x_xian='"&cstr(xname)&"'"
rsx.open sqlx,conn,1,1
If rsx.bof and rsx.eof then
showxids="@"
else
showxids=rsx("z_id")
end if
rsx.close
set rsx=nothing
End Function

'根据ID 找线路
Function showxnas(x_id)
set rsx=Server.CreateObject("ADODB.RecordSet")
sqlx="select * from [xian] where x_id="&x_id&""
rsx.open sqlx,conn,1,1
If rsx.bof and rsx.eof then
showxnas="@"
else
showxnas=rsx("x_xian")
end if
rsx.close
set rsx=nothing
End Function

'站点存在否
Function zdcz(zhaniadx)
set rszdcz=Server.CreateObject("ADODB.RecordSet")
sqlzdcz="select * from [zhan] where z_zhan='"&zhaniadx&"'"
rszdcz.open sqlzdcz,conn,1,3
if not rszdcz.eof then
zdcz="ture"
else
zdcz="kong"
end if
rszdcz.close
set rszdcz=nothing
End Function

'线路存在否
Function xlcz(xian)
set rsxlcz=Server.CreateObject("ADODB.RecordSet")
sqlxlcz="select * from [xian] where x_xian='"&xian&"'"
rsxlcz.open sqlxlcz,conn,1,3
if not rsxlcz.eof then
xlcz="ture"
else
xlcz="kong"
end if
rsxlcz.close
set rsxlcz=nothing
End Function

'添加站点
Function adzd(zhaniadx,zhanping)
if len(zhanping)<5 then zhanping=x(zhaniadx)&"_"&c(zhaniadx)
lines="---->添加站点:<span>"&zhaniadx&"</span>"&vbcrlf
Resp=Resp&""&lines&"<br>"&vbcrlf
set rsadzd = Server.CreateObject("ADODB.Recordset")
sqladzd="select * from zhan"
rsadzd.open sqladzd,conn,1,3
rsadzd.addnew
rsadzd("z_zhan")=zhaniadx
rsadzd("z_ping")="-"
rsadzd("z_xian")="-"
rsadzd("z_char")=zhanping
rsadzd.update
rsadzd.close
set rsadzd=nothing
End Function

'添加线路
Function adxl(xian,shuo,zhan)
if left(zhan,1)<>"-" then zhan="-"&zhan
if right(zhan,1)<>"-" then zhan=zhan&"-"
zhanadlx=split(zhan,"-")
xianping="-"
for iadx=0 to UBound(zhanadlx)
zhaniadx=zhanadlx(iadx)
if len(zhaniadx)>1 then
if zdcz(zhaniadx)="ture" then
else
zhanping=c(zhaniadx)&"-"&x(zhaniadx)
call adzd(zhaniadx,zhanping)
end if
xianping=xianping&""&showzids(zhaniadx)&"-"
end if
next
if xlcz(xian)="ture" then
else
set rsadxl = Server.CreateObject("ADODB.Recordset")
sqladxl="select * from xian"
rsadxl.open sqladxl,conn,1,3
rsadxl.addnew
rsadxl("x_xian")=trim(xian)
rsadxl("x_shuo")=trim(shuo)
rsadxl("x_zhan")=trim(zhan)
rsadxl("x_ping")=xianping
rsadxl.update
xian_id=rsadxl("x_id")
rsadxl.close
set rsadxl=nothing
Resp=Resp&"添加线路:<span>"&xian&"</span><br>"&vbcrlf
end if
gxzds=split(trim(zhan),"-")
gxzdp=split(trim(xianping),"-")
for izd=0 to UBound(gxzds)
if len(gxzds(izd))>0 then
gxzdsizd=cstr(gxzds(izd))
call gxzd(gxzdsizd,xian,xian_id)
else
end if
next
End Function

'就删除线路
Function dexl(x_xian,x_id)
set rsdelxl = Server.CreateObject("ADODB.Recordset")
 Sqldelxl="Update xian Set x_zhan='-',x_ping='-' Where x_id="&x_id
 Conn.ExeCute Sqldelxl
lines="删除线路,<span>"&x_xian&"</span>"
Resp=Resp&""&lines&"<br>"&vbcrlf
call thxl(x_xian,x_id)
'call delkx()
Resp=Resp&"</td></tr><tr><td>线路<span>"&x_xian&"</span>删除<br>"&vbcrlf
End Function

'就删除站点
Function dezd(z_zhan,z_id)
set rsdelxl = Server.CreateObject("ADODB.Recordset")
 Sqldelxl="Update zhan Set z_xian='-',z_ping='-' Where z_id="&z_id
 Conn.ExeCute Sqldelxl
lines="删除站点,<span>"&z_zhan&"</span>"
Resp=Resp&""&lines&"<br>"&vbcrlf
call thzd(z_zhan,z_id)
Resp=Resp&"</td></tr><tr><td >站点<span>"&z_zhan&"</span>删除<br>"&vbcrlf
End Function

'删除站点后替换线路数组
Function thzd(z_zhan,z_id)
 q1="-"&z_zhan&"-"
 q2="-"&z_id&"-"
 if z_id="" then z_id=showzids(z_zhan)
 Set Rsthzd=Server.CreateObject("adodb.Recordset")
 Sqlthzd="Select * from xian where x_zhan like '%"&q1&"%' or x_ping like '%"&q2&"%'"
 Rsthzd.open Sqlthzd,conn,1,1
 Do while NOT Rsthzd.eof
 x_zhan=replace(rsthzd("x_zhan"),q1,"-")
if z_id<>"@" then
 x_ping=replace(rsthzd("x_ping"),q2,"-")
end if
 x_id=rsthzd("x_id")
 set rsth=Server.CreateObject("ADODB.recordset")
 sqlth="select * from xian where x_id="&x_id&""
 rsth.open sqlth,conn,3,3
 if Not Rsth.EOF Then
 rsth("x_zhan")=x_zhan
if z_id<>"@" then
 rsth("x_ping")=x_ping
end if
 rsth.update
 lines="--->更新线路,<span>"&rsth("x_xian")&"</span>"&vbcrlf
Resp=Resp&""&lines&"<br>"&vbcrlf
 rsth.close
 set rsth=nothing
 end if
 Rsthzd.Movenext
 Loop
 rsthzd.close
 set rsthzd=nothing
End Function

'增加线路后更新站点
Function gxzd(z_name,x_xian,x_id)
 set rsth=Server.CreateObject("ADODB.recordset")
 sqlth="select * from zhan where z_zhan='"&z_name&"'"
 rsth.open sqlth,conn,3,3
 if Not Rsth.EOF Then
 if instr(rsth("z_xian"),x_xian)>0 then
 'Resp=Resp&"站点<span>"&rsth("z_zhan")&"</span>无需更新<br>"&vbcrlf
 else
 rsth("z_xian")=rsth("z_xian")&""&x_xian&"-"
 rsth("z_ping")=rsth("z_ping")&""&x_id&"-"
 end if
 rsth.update
rsthz_zhan=rsth("z_zhan")
 rsth.close
 set rsth=nothing
 lines="--->更新站点,<span>"&rsthz_zhan&"</span>"&vbcrlf
Resp=Resp&""&lines&"<br>"&vbcrlf
 end if
End Function

'删除线路后替换站点数组
Function thxl(x_xian,x_id)
 q1="-"&x_xian&"-"
 q2="-"&x_id&"-"
 Set Rsthzd=Server.CreateObject("adodb.Recordset")
 Sqlthzd="Select * from zhan where z_zhan like '%"&q1&"%' or z_ping like '%"&q2&"%'"
 Rsthzd.open Sqlthzd,conn,1,1
 Do while NOT Rsthzd.eof
 z_xian=replace(rsthzd("z_xian"),q1,"-")
 z_ping=replace(rsthzd("z_ping"),q2,"-")
 z_id=rsthzd("z_id")
 set rsth=Server.CreateObject("ADODB.recordset")
 sqlth="select * from zhan where z_id="&z_id&""
 rsth.open sqlth,conn,3,3
 if Not Rsth.EOF Then
 rsth("z_xian")=z_xian
 rsth("z_ping")=z_ping
 rsth.update
 lines="--->更新站点,<span>"&rsth("z_zhan")&"</span>"&vbcrlf
Resp=Resp&""&lines&"<br>"&vbcrlf
 rsth.close
 set rsth=nothing
 else
'Resp=Resp&"<span>出错：指定的记录不存在</span><br>"&vbcrlf
 end if
 Rsthzd.Movenext
 Loop
 rsthzd.close
 set rsthzd=nothing
End Function

'删除空线路
Function kongxl()
 Set Rskxl=Server.CreateObject("adodb.Recordset")
 Sqlkxl="Select * from xian"
 Rskxl.open Sqlkxl,conn,1,1
 Do while NOT Rskxl.eof
 x_xian=rskxl("x_xian")
 x_zhan=rskxl("x_zhan")
 x_id=rskxl("x_id")
 if len(x_zhan)<3 then
 lines="--->删除线路,<span>"&x_xian&"</span>"&vbcrlf
Resp=Resp&""&lines&"<br>"&vbcrlf
 set rsdelxl = Server.CreateObject("ADODB.Recordset")
 Sqldelxl="Delete * from xian where x_id="&x_id
 conn.execute Sqldelxl  
 end if
 Rskxl.Movenext
 Loop
 rskxl.close
 set rskxl=nothing
End Function

'删除空站点
Function kongzd()
 Set Rskxl=Server.CreateObject("adodb.Recordset")
 Sqlkxl="Select * from zhan"
 Rskxl.open Sqlkxl,conn,1,1
 Do while NOT Rskxl.eof
 z_xian=rskxl("z_xian")
 z_zhan=rskxl("z_zhan")
 z_id=rskxl("z_id")
 if len(z_xian)<3 then
 set rsdelxl = Server.CreateObject("ADODB.Recordset")
 Sqldelxl="Delete * from zhan where z_id="&z_id
 conn.execute Sqldelxl  
 lines="--->删除站点,<span>"&z_zhan&"</span>"&vbcrlf
Resp=Resp&""&lines&"<br>"&vbcrlf
 end if
 Rskxl.Movenext
 Loop
 rskxl.close
 set rskxl=nothing
End Function

'站点更名后替换线路数组
Function rezd(ozhan,nzhan)
 q1="-"&ozhan&"-"
 q2="-"&nzhan&"-"
 Set Rsthzd=Server.CreateObject("adodb.Recordset")
 Sqlthzd="Select * from xian where x_zhan like '%"&q1&"%'"
 Rsthzd.open Sqlthzd,conn,1,1
 Do while NOT Rsthzd.eof
 x_zhan=replace(rsthzd("x_zhan"),q1,q2)
 x_id=rsthzd("x_id")
 set rsth=Server.CreateObject("ADODB.recordset")
 sqlth="select * from xian where x_id="&x_id&""
 rsth.open sqlth,conn,3,3
 if Not Rsth.EOF Then
 rsth("x_zhan")=x_zhan
 rsth.update
 lines="--->更新线路,<span>"&rsth("x_xian")&"</span>"&vbcrlf
Resp=Resp&""&lines&"<br>"&vbcrlf
 rsth.close
 set rsth=nothing
 end if
 Rsthzd.Movenext
 Loop
 rsthzd.close
 set rsthzd=nothing
End Function

'删除线路后替换站点数组
Function rexl(oxian,nxian)
 q1="-"&oxian&"-"
 q2="-"&nxian&"-"
 Set Rsthzd=Server.CreateObject("adodb.Recordset")
 Sqlthzd="Select * from zhan where z_xian like '%"&q1&"%'"
 Rsthzd.open Sqlthzd,conn,1,1
 Do while NOT Rsthzd.eof
 z_xian=replace(rsthzd("z_xian"),q1,q2)
 z_id=rsthzd("z_id")
 set rsth=Server.CreateObject("ADODB.recordset")
 sqlth="select * from zhan where z_id="&z_id&""
 rsth.open sqlth,conn,3,3
 if Not Rsth.EOF Then
 rsth("z_xian")=z_xian
 rsth.update
 lines="--->更新站点,<span>"&rsth("z_zhan")&"</span>"&vbcrlf
Resp=Resp&""&lines&"<br>"&vbcrlf
 rsth.close
 set rsth=nothing
 end if
 Rsthzd.Movenext
 Loop
 rsthzd.close
 set rsthzd=nothing
End Function

'站点合并后替换线路数组
Function hbzdh(ozhan,nzhan)
 q1="-"&ozhan&"-"
 q2="-"&nzhan&"-"
 i1="-"&showzids(ozhan)&"-"
 i2="-"&showzids(nzhan)&"-"
 Set Rsthzd=Server.CreateObject("adodb.Recordset")
 Sqlthzd="Select * from xian where x_zhan like '%"&q1&"%'"
 Rsthzd.open Sqlthzd,conn,1,1
 Do while NOT Rsthzd.eof
 x_zhan=replace(rsthzd("x_zhan"),q1,q2)
 x_ping=replace(rsthzd("x_ping"),i1,i2)
 x_id=rsthzd("x_id")
 set rsth=Server.CreateObject("ADODB.recordset")
 sqlth="select * from xian where x_id="&x_id&""
 rsth.open sqlth,conn,3,3
 if Not Rsth.EOF Then
 rsth("x_zhan")=x_zhan
 rsth("x_ping")=x_ping
 rsth.update
 lines="--->更新线路,<span>"&rsth("x_xian")&"</span>"&vbcrlf
Resp=Resp&""&lines&"<br>"&vbcrlf
 rsth.close
 set rsth=nothing
 end if
 Rsthzd.Movenext
 Loop
 rsthzd.close
 set rsthzd=nothing
End Function

'站点求所有线以及ID
Function zallxian(zhan)
 q1="-"&zhan&"-"
xxian="-"
xidid="-"
 Set Rsthzd=Server.CreateObject("adodb.Recordset")
 Sqlthzd="Select * from xian where x_zhan like '%"&q1&"%'"
 Rsthzd.open Sqlthzd,conn,1,1
 Do while NOT Rsthzd.eof
 x_xian=rsthzd("x_xian")
 x_id=rsthzd("x_id")
xxian=xxian&x_xian&"-"
xidid=xidid&x_id&"-"
 Rsthzd.Movenext
 Loop
zallxian=xxian&"|"&xidid
 rsthzd.close
 set rsthzd=nothing
End Function

'检修站点数组
Function checkz()
 Set Rsthzd=Server.CreateObject("adodb.Recordset")
 Sqlthzd="Select * from zhan"
 Rsthzd.open Sqlthzd,conn,1,1
 Do while NOT Rsthzd.eof
 z_zhan=rsthzd("z_zhan")
 z_id=rsthzd("z_id")
 z_xian=rsthzd("z_xian")
 z_ping=rsthzd("z_ping")
 zz_all=zallxian(z_zhan)
 zzxian=split(zz_all,"|")(0)
 zzping=split(zz_all,"|")(1)
if z_xian=zzxian and z_ping=zzping then
 'ok=============>
else
 lines="--->站点<span>"&z_zhan&"</span>已经更新！"&vbcrlf
 Resp=Resp&""&lines&"<br>"&vbcrlf
 set rsdelxl = Server.CreateObject("ADODB.Recordset")
 Sqldelxl="Update zhan Set z_xian='"&zzxian&"',z_ping='"&zzping&"' Where z_id="&z_id
 Conn.ExeCute Sqldelxl
end if
 Rsthzd.Movenext
 Loop
 rsthzd.close
 set rsthzd=nothing
End Function

'检修线路数组
Function xztozz()
 Set Rsthzd=Server.CreateObject("adodb.Recordset")
 Sqlthzd="Select * from xian"
 Rsthzd.open Sqlthzd,conn,1,1
 Do while NOT Rsthzd.eof
 x_zhan=rsthzd("x_zhan")
 xxzhan=split(x_zhan,"-")
 numx=ubound(xxzhan)
 for ix=0 to numx
 xxzhanix=xxzhan(ix)
 if zdcz(xxzhanix)="kong" then
 if len(xxzhanix)>1 then
 call adzd(xxzhanix,"")
 lines="--->站点<span>"&xxzhanix&"</span>增加！"&vbcrlf
 Resp=Resp&""&lines&"<br>"&vbcrlf
 else
 lines="--->站点<span>"&xxzhanix&"</span>太短！"&vbcrlf
 if len(xxzhanix)>0 then
 Resp=Resp&""&lines&"<br>"&vbcrlf
 call thzd(xxzhanix,"")
 end if
 end if
 end if
 next
 Rsthzd.Movenext
 Loop
 rsthzd.close
 set rsthzd=nothing
End Function

'检修线路数组
Function checkx()
 Set Rsthzd=Server.CreateObject("adodb.Recordset")
 Sqlthzd="Select * from xian"
 Rsthzd.open Sqlthzd,conn,1,1
 Do while NOT Rsthzd.eof
 x_xian=rsthzd("x_xian")
 x_id=rsthzd("x_id")
 x_zhan=rsthzd("x_zhan")
 x_ping=rsthzd("x_ping")
 xxzhan=split(x_zhan,"-")
 numx=ubound(xxzhan)
 xxping="-"
 xxzzzz="-"
 for ix=0 to numx
 xxzhanix=xxzhan(ix)
 if len(xxzhanix)>1 then
 idid=idid+1
 if zdcz(xxzhanix)="kong" then
 call adzd(xxzhanix,"")
 end if
 xxpingix=showzids(xxzhanix)
 xxping=xxping&xxpingix&"-"
 xxzzzz=xxzzzz&xxzhanix&"-"
 end if
 next
 if x_ping=xxping then
 else
 lines="--->线路<span>"&x_xian&"</span>已经更新！"&vbcrlf
 Resp=Resp&""&lines&"<br>"&vbcrlf
 set rsdelxl = Server.CreateObject("ADODB.Recordset")
 Sqldelxl="Update xian Set x_zhan='"&xxzzzz&"',x_ping='"&xxping&"' Where x_id="&x_id
 Conn.ExeCute Sqldelxl
 end if
 Rsthzd.Movenext
 Loop
 rsthzd.close
 set rsthzd=nothing
End Function

'线路求所有站点以及ID
Function xallzhan(xian)
 q1="-"&xian&"-"
xxian="-"
xidid="-"
 Set Rsthzd=Server.CreateObject("adodb.Recordset")
 Sqlthzd="Select * from zhan where z_xian like '%"&q1&"%'"
 Rsthzd.open Sqlthzd,conn,1,1
 Do while NOT Rsthzd.eof
 z_zhan=rsthzd("z_zhan")
 z_id=rsthzd("z_id")
xxian=xxian&z_zhan&"-"
xidid=xidid&z_id&"-"
 Rsthzd.Movenext
 Loop
xallzhan=xxian&"|"&xidid
 rsthzd.close
 set rsthzd=nothing
End Function

Function qubie(news,olds)
 oldss=split(olds,"-")
 iod=ubound(oldss)
 qubie="-"
 for io=0 to iod
 older="-"&oldss(io)&"-"
 if instr(news,older)>0 or len(older)<4 then
 else
 qubie=qubie&oldss(io)&"-"
 end if
 next
End Function

Function upzhan(zhanss)
 zhansd=split(zhanss,"-")
 izd=ubound(zhansd)
 for iz=0 to izd
 if len(zhansd(iz))>1 then
 z_id=showzids(zhansd(iz))
 zz_alls=zallxian(zhansd(iz))
 zzxians=split(zz_alls,"|")(0)
 zzpings=split(zz_alls,"|")(1)
 lines="--->站点<span>"&zhansd(iz)&"</span>已经更新！"&vbcrlf
 Resp=Resp&""&lines&"<br>"&vbcrlf
 set rsjzd = Server.CreateObject("ADODB.Recordset")
 Sqljzd="Update zhan Set z_xian='"&zzxians&"',z_ping='"&zzpings&"' Where z_id="&z_id
 Conn.ExeCute Sqljzd
 end if
 next
End Function
%>
